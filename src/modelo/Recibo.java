package modelo;

public class Recibo {
    private int numRecibo;
    private String nombre;
    private String domicilio;
    private String fecha;
    private int tipoServicio;
    private float CostoKilowatts;
    private float kilowattsConsumidos;

    public Recibo(int numRecibo, String nombre, String domicilio, int tipoServicio, float CostoKilowatts, float kilowattsConsumidos, String fecha) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipoServicio = tipoServicio;
        this.CostoKilowatts = CostoKilowatts;
        this.kilowattsConsumidos = kilowattsConsumidos;
        this.fecha= fecha;
    }

    public Recibo(Recibo aux) {
        this.numRecibo = aux.numRecibo;
        this.nombre = aux.nombre;
        this.domicilio = aux.domicilio;
        this.tipoServicio = aux.tipoServicio;
        this.CostoKilowatts = aux.CostoKilowatts;
        this.kilowattsConsumidos = aux.kilowattsConsumidos;
        this.fecha = aux.fecha;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostoKilowatts() {
        return CostoKilowatts;
    }

    public void setCostoKilowatts(float CostoKilowatts) {
        this.CostoKilowatts = CostoKilowatts;
    }

    public float getKilowattsConsumidos() {
        return kilowattsConsumidos;
    }

    public void setKilowattsConsumidos(float kilowattsConsumidos) {
        this.kilowattsConsumidos = kilowattsConsumidos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    public Recibo() {
        this.numRecibo = 0;
        this.nombre = "";
        this.domicilio = "";
        this.tipoServicio = 0;
        this.CostoKilowatts = 0.0f;
        this.kilowattsConsumidos = 0.0f;
    }
    
    public float calcularSubtotal(){
        return (float)(CostoKilowatts * kilowattsConsumidos);
    }
    
    public float calcularImpuesto(){
        return (calcularSubtotal() * 0.16f);
    }
    
    public float calcularTotal(){
        return (calcularSubtotal() + calcularImpuesto());
    }
    
    public float calcularCosto(){
        if(this.tipoServicio==1){
            return 2.0f;
        }
        else if(this.tipoServicio==2){
            return 3.0f;
        }
        else{
            return 5.0f;
        }
    }
}
