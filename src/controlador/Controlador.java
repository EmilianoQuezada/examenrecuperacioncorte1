package controlador;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.Recibo;
import vista.dlgRecibo;

public class Controlador implements ActionListener {
    
    private Recibo recibo;
    private dlgRecibo vista;

    public Controlador(Recibo recibo, dlgRecibo vista) {
        this.recibo = recibo;
        this.vista = vista;
        
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
        this.vista.btnLimpiar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnCerrar.addActionListener(this);
    }
    
    public void iniciarVista(){
        this.vista.setTitle("Nomina");
        this.vista.setSize(500, 450);
        this.vista.setVisible(true);
    }
    
    public void limpiar(){
        this.vista.txtNumRecibo.setText("");
        this.vista.txtFecha.setText("");
        this.vista.txtNombre.setText("");
        this.vista.txtDomicilio.setText("");
        this.vista.txtCostoKilo.setText("");
        this.vista.txtKiloConsumido.setText("");
        this.vista.cbTipoServicio.setSelectedIndex(0);
    }
    
    public boolean isVacio(){
        if(this.vista.txtNumRecibo.getText().equals("")|| this.vista.txtFecha.getText().equals("")|| this.vista.txtNombre.getText().equals("")|| this.vista.txtDomicilio.getText().equals("")|| this.vista.txtCostoKilo.getText().equals("") || this.vista.txtKiloConsumido.getText().equals("")|| this.vista.cbTipoServicio.getSelectedIndex()==0){
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.vista.btnNuevo){
            this.vista.txtNumRecibo.enable(true);
            this.vista.txtFecha.enable(true);
            this.vista.txtNombre.enable(true);
            this.vista.txtDomicilio.enable(true);
            this.vista.txtCostoKilo.enable(true);
            this.vista.txtKiloConsumido.enable(true);
            
            this.vista.cbTipoServicio.setEnabled(true);
            this.vista.btnGuardar.setEnabled(true);
            this.vista.btnLimpiar.setEnabled(true);
            this.vista.btnCancelar.setEnabled(true);
        }
        
        if(e.getSource()==this.vista.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource()==this.vista.btnCancelar){
            limpiar();
            this.vista.txtNumRecibo.enable(false);
            this.vista.txtFecha.enable(false);
            this.vista.txtNombre.enable(false);
            this.vista.txtDomicilio.enable(false);
            this.vista.txtKiloConsumido.enable(false);
            this.vista.txtCostoKilo.enable(false);
            
            this.vista.cbTipoServicio.setEnabled(false);
            this.vista.btnGuardar.setEnabled(false);
            this.vista.btnLimpiar.setEnabled(false);
            this.vista.btnCancelar.setEnabled(false);
        }
        
        if(e.getSource()==this.vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista, "¿Desea Cerrar el programa?",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        
        if(e.getSource()==this.vista.btnGuardar){
            if(isVacio()==true){
                JOptionPane.showMessageDialog(vista, "Imposible guardar, hay campos vacios");
            }
            else{
                try{
                    this.recibo.setNumRecibo(Integer.parseInt(this.vista.txtNumRecibo.getText()));
                    this.recibo.setFecha(this.vista.txtFecha.getText());
                    this.recibo.setNombre(this.vista.txtNombre.getText());
                    this.recibo.setDomicilio(this.vista.txtDomicilio.getText());
                    this.recibo.setTipoServicio(this.vista.cbTipoServicio.getSelectedIndex());
                    this.recibo.setCostoKilowatts(Float.parseFloat(this.vista.txtCostoKilo.getText()));
                    this.recibo.setKilowattsConsumidos(Float.parseFloat(this.vista.txtKiloConsumido.getText()));  
                    this.vista.btnMostrar.setEnabled(true);
                    JOptionPane.showMessageDialog(vista, "Guardado con exito");
                    limpiar();
                    
                }
                catch(NumberFormatException ex){
                        JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ ex.getMessage());
                    }
            }
            
        }
        
        if(e.getSource()==this.vista.btnMostrar){
            this.vista.txtNumRecibo.setText(String.valueOf(this.recibo.getNumRecibo()));
            this.vista.txtFecha.setText(this.recibo.getFecha());
            this.vista.txtNombre.setText(this.recibo.getNombre());
            this.vista.txtDomicilio.setText(this.recibo.getDomicilio());
            this.vista.cbTipoServicio.setSelectedItem(this.recibo.getTipoServicio());
            this.vista.txtCostoKilo.setText(String.valueOf(this.recibo.calcularCosto()));
            this.vista.txtKiloConsumido.setText(String.valueOf(this.recibo.getKilowattsConsumidos()));
            
            this.vista.txtSubtotal.setText(String.valueOf(this.recibo.calcularSubtotal()));
            this.vista.txtImpuesto.setText(String.valueOf(this.recibo.calcularImpuesto()));
            this.vista.txtTotal.setText(String.valueOf(this.recibo.calcularTotal()));
        }
    }
    
    public static void main(String[] args) {
        Recibo recibo = new Recibo();
        dlgRecibo vista= new dlgRecibo(new JFrame(), true);
        Controlador contra = new Controlador(recibo, vista);
        contra.iniciarVista();
    }
}
